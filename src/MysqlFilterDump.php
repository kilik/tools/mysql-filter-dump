<?php

include_once "Dump.php";

class MysqlFilterDump
{

    const DEFAULT_CONFIG_CLASS = 'config';

    /**
     * Global Config
     *
     * @var array
     */
    private $config;

    /**
     * Dump config
     *
     * @var array
     */
    private $dumpConfig;

    /**
     * Verbose mode
     *
     * @var int
     */
    private $verbose = 1;

    /**
     * Entry.
     *
     * @param $argc
     * @param $argv
     *
     * @throws \Exception
     */
    public function exec($argc, $argv)
    {
        $configClass = static::DEFAULT_CONFIG_CLASS;

        $todoDumps = [];
        $limitTables = [];

        $displayHelp = false;
        $dumpAll = false;
        $showConfig = false;
        $maxRevertRows = 50;
        $maxDeep = 100;

        for ($i = 1; $i < $argc; ++$i) {
            if ($argv[$i][0] != '-') {
                $todoDumps[]
                    = $argv[$i];
            } else {
                switch ($argv[$i]) {
                    case '--help':
                        $displayHelp = true;
                        break;
                    case '--all':
                        $dumpAll = true;
                        break;
                    case '-v':
                        $this->verbose = 1;
                        break;
                    case '-vv':
                        $this->verbose = 2;
                        break;
                    case '-vvv':
                        $this->verbose = 3;
                        break;
                    case '-vvvv':
                        $this->verbose = 4;
                        break;
                    case '--config':
                        $configClass[$argv[++$i]];
                        break;
                    case '--tables':
                        $limitTables = explode(',', $argv[++$i]);
                        break;
                    case '--show-config':
                        $showConfig = true;
                        break;
                    case '--max-revert-rows':
                        $maxRevertRows = $argv[++$i];
                        break;
                    case '--max-deep':
                        $maxDeep = $argv[++$i];
                        break;
                }
            }
        }

        // check if class exists
        $classFilePath = 'config/'.$configClass.'.php';
        if (!file_exists($classFilePath)) {
            throw new \Exception('config class '.$configClass.' not found');
        }

        include_once $classFilePath;
        $this->config = new $configClass();

        $this->configureExtendedDumps();

        if ($dumpAll) {
            $todoDumps = [];
            foreach ($this->config->dumps as $dumpName => $dump) {
                if (!isset($dump['manual']) || $dump['manual'] != true) {
                    $todoDumps[] = $dumpName;
                }
            }
        }

        // no dumps to do or want help ?
        if ($displayHelp || count($todoDumps) == 0) {
            echo 'syntax: mysqlfilterdump <dumps> '.PHP_EOL;
            echo ' --tables table1,table2       limit dump to those tables (default: all tables)'.PHP_EOL;
            echo ' --show-config                show dumps configuration'.PHP_EOL;
            echo 'dumps:';
            foreach ($this->config->dumps as $dumpName => $dump) {
                echo ' '.$dumpName;
            }
            echo PHP_EOL;

            return;
        }

        // need to show config ?
        if ($showConfig) {
            foreach ($todoDumps as $todoDump) {
                $message = "Dump $todoDump";
                echo $message.PHP_EOL;
                echo str_repeat('=', strlen($message)).PHP_EOL.PHP_EOL;

                $dump = $this->config->dumps[$todoDump];
                echo "database       : ".(isset($dump['database']) ? $dump['database'] : 'n/a').PHP_EOL;
                echo "connection     : ".(isset($dump['connection']) ? $dump['connection'] : 'n/a').PHP_EOL;
                foreach (['noReverseTables', 'noReverseLinks', 'fullTablesData', 'ignoreTables', 'ignoreTablesData'] as $item) {
                    echo sprintf("%-15s: ", $item).(isset($dump[$item]) ? PHP_EOL.' - '.implode(PHP_EOL.' - ', $dump[$item]) : '').PHP_EOL;
                }
            }

            return;
        }

        $basePath = '/dumps';

        if (!is_dir($basePath)) {
            echo $basePath.' directory created'.PHP_EOL;
            mkdir($basePath);
        }

        if (count($todoDumps) > 0) {
            foreach ($todoDumps as $todoDump) {
                $makeDump = false;
                if (!isset($this->config->dumps[$todoDump])) {
                    echo 'ignoring unknown database '.$todoDump.PHP_EOL;
                    continue;
                }

                $dumpConfig = $this->config->dumps[$todoDump];
                if (!isset($dumpConfig['connection'])) {
                    echo 'skipping dump '.$todoDump.' without connection'.PHP_EOL;
                    continue;
                }
                if (!isset($dumpConfig['database'])) {
                    echo 'skipping dump '.$todoDump.' without database name'.PHP_EOL;
                    continue;
                }
                if (!isset($this->config->connections[$dumpConfig['connection']])) {
                    echo 'skipping dump '.$todoDump.' with missing connection '.$dumpConfig['connection'].PHP_EOL;
                }
                $connection = $this->config->connections[$dumpConfig['connection']];

                $dumpPath = $basePath.'/'.date('Ymd');
                $dumpFile = $dumpPath.'/'.$dumpConfig['connection'].'.'.$todoDump.'.sql';

                try {
                    $dump = new Dump();
                    $dump->setVerbose($this->verbose);
                    $dump->setFile($dumpFile);
                    $dump->setConfig($dumpConfig, $connection);
                    $dump->setMaxRevertRows($maxRevertRows);
                    $dump->setMaxDeep($maxDeep);
                    $dump->export($limitTables);
                } catch (Exception $e) {
                    echo $e->getMessage();
                }
            }
        }
    }

    /**
     * Configure extended dumps
     */
    public function configureExtendedDumps()
    {
        foreach ($this->config->dumps as $dumpName => $dumpConfig) {
            if (isset($dumpConfig['extends'])) {
                $extendFrom = $dumpConfig['extends'];
                if (!isset($this->config->dumps[$extendFrom])) {
                    throw new \Exception(sprintf('dump %s could not extends from non existing %s dump', $dumpName, $extendFrom));
                }
                // merge / replace config
                // clone original
                $dump = $this->config->dumps[$extendFrom];

                // replace values
                foreach (['database', 'connection'] as $item) {
                    if (isset($dumpConfig[$item])) {
                        $dump[$item] = $dumpConfig[$item];
                    }
                }
                // merges basics arrays
                foreach (['noReverseTables', 'noReverseLinks', 'fullTablesData', 'ignoreTables', 'ignoreTablesData'] as $item) {
                    if (isset($dump[$item]) && isset($dumpConfig[$item])) {
                        $dump[$item] = array_unique(array_merge($dump[$item], $dumpConfig[$item]));
                    }
                }

                // merges complex arrays
                foreach (['limitTables', 'forceValues', 'forcePrimaryKeys', 'forceForeignKeys'] as $item) {
                    if (isset($dump[$item]) && isset($dumpConfig[$item])) {
                        $dump[$item] = array_replace($dump[$item], $dumpConfig[$item]);
                    }
                }

                // merges anonymizer arrays
                foreach (['fields', 'tables'] as $item) {
                    if (isset($dumpConfig[$item]['anonymizer'][$item]) && isset($dumpConfig['anonymizer'][$item])) {
                        $dump[$item] = array_replace($dumpConfig['anonymizer'][$item], $dumpConfig['anonymizer'][$item]);
                    }
                }

                $this->config->dumps[$dumpName] = $dump;
            }
        }
    }

}
