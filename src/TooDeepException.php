<?php

class TooDeepException extends Exception
{

    /**
     * @var array
     */
    private $stack;

    /**
     * @param string    $message [optional] The Exception message to throw.
     * @param int       $code [optional] The Exception code.
     * @param Throwable $previous [optional] The previous throwable used for the exception chaining.
     * @param array     $stack
     */
    public function __construct($message = "", $code = 0, Throwable $previous = null, $stack)
    {
        parent::__construct($message, $code, $previous);

        $this->stack = $stack;
    }

    /**
     * @return array
     */
    public function getStack()
    {
        return $this->stack;
    }
}
