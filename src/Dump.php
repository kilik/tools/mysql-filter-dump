<?php

include_once "Table.php";
include_once "FileException.php";
include_once "ForeignKey.php";
include_once "LoremIpsumFilter.php";
include_once "ProcessException.php";
include_once "TooDeepException.php";
include_once "TooManyResultsException.php";

class Dump
{
    const ANONYMIZER_ALPHA = 'ALPHA';
    const ANONYMIZER_NUM = 'NUM';
    const ANONYMIZER_ALPHA_NUM = 'ALPHA_NUM';
    const ANONYMIZER_PHONE = 'PHONE';
    const ANONYMIZER_LOREM_IPSUM = 'LOREM_IPSUM';

    const ANONYMIZERS
        = [
            self::ANONYMIZER_ALPHA,
            self::ANONYMIZER_NUM,
            self::ANONYMIZER_ALPHA_NUM,
            self::ANONYMIZER_PHONE,
            self::ANONYMIZER_LOREM_IPSUM,
        ];

    const ANONYMIZE_FROM = 'aeiouybcdfghjklmnpqrstvwxz';
    const ANONYMIZE_TO = 'eiouoecbfghgcmmmpcrstvvxzb';

    const ANONYMIZE_NUM_FROM = '0123456789';
    const ANONYMIZE_NUM_TO = '0222346899';

    /**
     * Verbose mode
     *
     * @var int
     */
    private $verbose = 1;

    /**
     * File path to store export
     *
     * @var string
     */
    private $file;

    /**
     * @var Table[]
     */
    private $schema = [];

    /**
     * Connection to MySQL DB
     *
     * @var PDO
     */
    private $link;

    /**
     * Cache known id (indexed by table / id / true|false)
     *
     * @var array
     */
    private $exportedId;

    /**
     * Nb Exported rows (in sequence)
     *
     * @var int
     */
    private $nbExportedRows;

    /**
     * Total Exported rows
     *
     * @var int
     */
    private $totalExportedRows;

    /**
     * @var array
     */
    private $anonymize;

    /**
     * @var array
     */
    private $anonymizeNum;

    /**
     * Config
     *
     * @var array
     */
    private $config;

    /**
     * Connection parameters
     *
     * @var array
     */
    private $connectionConfig;

    /**
     * Too many results tables (keep a trace of too many results)
     *
     * @var array
     */
    private $tooManyResults = [];

    /**
     * Define max revert rows
     * @var int
     */
    private $maxRevertRows = 50;

    /**
     * Define max deep (recursive links followed)
     *
     * @var int
     */
    private $maxDeep = 100;

    /**
     * Total queries
     *
     * @var int
     */
    private $nbQueries = 0;

    /**
     * Nb of rows to dump in current section
     *
     * @var int
     */
    private $sectionRowsToDump = 0;

    /**
     * Nb of rows dumped in current section
     *
     * @var int
     */
    private $sectionRowsDumped = 0;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->anonymize = $this->stringsToArray(
            static::ANONYMIZE_FROM.strtoupper(static::ANONYMIZE_FROM),
            static::ANONYMIZE_TO.strtoupper(static::ANONYMIZE_TO)
        );
        $this->anonymizeNum = $this->stringsToArray(
            static::ANONYMIZE_NUM_FROM,
            static::ANONYMIZE_NUM_TO
        );
    }

    /**
     * Set file name (of export)
     *
     * @param string $file
     */
    public function setFile($file)
    {
        $this->file = $file;
    }

    /**
     * Set config
     *
     * @param array $config
     * @param array $connection
     */
    public function setConfig($config, $connection)
    {
        $config += [
            'charset' => 'utf8',
        ];

        $this->config = $config;
        $this->connectionConfig = $connection;

        $this->config += [
            'allData' => false,
        ];
    }

    /**
     * Set verbose mode
     *
     * @param int $verbose verbose level
     */
    public function setVerbose($verbose)
    {
        $this->verbose = $verbose;
    }

    /**
     * Transform a string into an array of chars
     *
     * @param $string
     * @return array
     */
    private function stringToArray($string)
    {
        $values = [];
        for ($i = 0; $i < strlen($string); $i++) {
            $values[] = $string[$i];
        }

        return $values;
    }

    /**
     * Transform 2 strings as key=>value char by char
     *
     * @param $keys
     * @param $values
     *
     * @return array
     */
    private function stringsToArray($keys, $values)
    {
        $result = [];

        for ($i = 0; $i < strlen($keys); $i++) {
            $result[$keys[$i]] = $values[$i];
        }

        return $result;
    }

    /**
     * Start export
     *
     * @param array $limitTables array of tables to limit export (if empty: no limitations)
     *
     * @throws \Exception
     */
    public function export($limitTables)
    {
        $dumpPath = dirname($this->file);

        if (!is_dir($dumpPath)) {
            @mkdir($dumpPath, 0755, true);
        }
        if (!is_dir($dumpPath)) {
            throw new FileException('dump path '.$dumpPath.' not exists and could not be created');
        }

        // prevent overwrite with rotations
        if (file_exists($this->file)) {
            unlink($this->file);
        }

        // connect to database
        $connectionUrl = sprintf(
            'mysql:dbname=%s;host=%s:%d;charset=%s',
            $this->config['database'],
            $this->connectionConfig['host'],
            $this->connectionConfig['port'],
            $this->config['charset']
        );

        $this->link = new PDO($connectionUrl, $this->connectionConfig['user'], $this->connectionConfig['password']);
        $this->exportSchema();
        $this->loadSchema();
        $this->exportData($limitTables);
    }

    /**
     * Export database schema
     */
    private function exportSchema()
    {
        // else, we can dump the database to disk
        $arguments = [
            '-u '.$this->connectionConfig['user'],
            '-h '.$this->connectionConfig['host'],
            '-P '.$this->connectionConfig['port'],
            '--no-data',
        ];

        $secretArguments = $arguments;
        $printableArguments = $arguments;
        $secretArguments[] = '-p'.$this->connectionConfig['password'];
        $printableArguments[] = '-p'.md5($this->connectionConfig['password']);

        if (isset($this->config['ignoreTables']) && is_array($this->config['ignoreTables']) && count($this->config['ignoreTables']) > 0) {
            $ignoreTables = '';
            foreach ($this->config['ignoreTables'] as $table) {
                $ignoreTables .= ' --ignore-table='.$table;
            }
            $arguments[] = $ignoreTables;
        }

        // dump database schema
        $cmd = 'mysqldump '.implode(' ', $secretArguments).' '.$this->config['database'].' > '.$this->file;
        $printableCmd = 'mysqldump '.implode(' ', $printableArguments).' '.$this->config['database'].' > '.$this->file;

        if ($this->verbose >= 1) {
            echo 'debug: '.$printableCmd.PHP_EOL;
        }

        system($cmd, $returnVal);

        if ($returnVal != 0) {
            throw new ProcessException('error dumping schema for '.$this->file);
        }
    }

    /**
     * Load schema
     *
     * @param string $dumpName
     */
    private function loadSchema()
    {
        $this->loadSchemaFromDb($this->config['database']);

        // force primary keys
        if (isset($this->config['forcePrimaryKeys'])) {
            foreach ($this->config['forcePrimaryKeys'] as $tableName => $keys) {
                $table = $this->schema[$tableName];
                foreach ($keys as $key) {
                    $table->addPrimaryKeyField($key);
                }
            }
        }

        // forced foreign keys ? (for bad schemas with missing FK)
        if (isset($this->config['forceForeignKeys'])) {
            foreach ($this->config['forceForeignKeys'] as $tableName => $fields) {
                $table = $this->schema[$tableName];
                foreach ($fields as $field => $key) {
                    if (count($key) != 2) {
                        throw new InvalidArgumentException('forceForeignKeys should by an array of 2 strings [tableName,FieldName]');
                    }

                    $fk = new ForeignKey();
                    $fk->setTable($tableName);
                    $fk->setField($field);
                    $fk->setForeignTable($key[0]);
                    $fk->setForeignField($key[1]);
                    $table->addForeignKey($fk);
                }
            }
        }

        // build reverse keys mapping
        $this->mapReverseKeys();
    }

    /**
     * Load schema
     *
     * @param string $dbName
     */
    private function loadSchemaFromDb($dbName)
    {
        $this->schema = [];

        $tableRows = $this->link->query('SHOW TABLES', PDO::FETCH_NUM);
        $this->nbQueries++;
        foreach ($tableRows->fetchAll() as $tableRow) {

            $table = new Table();
            $table->setName($tableRow[0]);

            // load primary key information
            $pkRows = $this->link->prepare(
                "SHOW KEYS FROM `".$table->getName()."` WHERE key_name = 'PRIMARY'"
            );
            $pkRows->execute();
            $this->nbQueries++;

            foreach ($pkRows->fetchAll(PDO::FETCH_ASSOC) as $pkRow) {
                $table->addPrimaryKeyField($pkRow['Column_name']);
            }

            // load foreign keys
            $fkRows = $this->link->prepare(
                "SELECT table_name, column_name,referenced_table_name,referenced_column_name 
FROM information_schema.key_column_usage
WHERE
    referenced_table_name IS NOT NULL
    AND table_schema = :database 
    AND table_name = :table"
            );
            $fkRows->execute([':database' => $dbName, ':table' => $table->getName()]);
            $this->nbQueries++;
            foreach ($fkRows->fetchAll(PDO::FETCH_ASSOC) as $fkRow) {
                $fk = new ForeignKey();
                $fk->setTable($table->getName());
                $fk->setField($fkRow['column_name']);
                $fk->setForeignTable($fkRow['referenced_table_name']);
                $fk->setForeignField($fkRow['referenced_column_name']);

                $table->addForeignKey($fk);
            }

            // indexed by name
            $this->schema[$table->getName()] = $table;
        }
    }

    /**
     * Map reverse keys from foreign keys
     */
    private function mapReverseKeys()
    {
        // build the reverse foreign keys
        foreach ($this->schema as $table) {
            foreach ($this->schema as $foreignTable) {
                foreach ($foreignTable->getForeignKeys() as $foreignKey) {
                    if ($foreignKey->getForeignTable() == $table->getName()) {
                        $table->addReverseForeignKey($foreignKey);
                    }
                }
            }
        }
    }

    /**
     * Export data from connection into dump
     *
     * @param array $limitTables limit export to those tables
     *
     * @throws Exception
     */
    public function exportData($limitTables)
    {
        try {
            $this->totalExportedRows = 0;
            $this->exportedId = [];
            $start = microtime(true);

            // for each table
            foreach ($this->schema as $table) {
                if (0 == count($limitTables) || in_array($table->getName(), $limitTables)) {
                    // table data filtered ?
                    if (isset($this->config['limitTables'][$table->getName()])) {
                        $this->nbExportedRows = 0;
                        $this->config['limitTables'][$table->getName()] += [
                            'disabled' => false,
                        ];
                        if (!$this->config['limitTables'][$table->getName()]['disabled']) {
                            if ($this->verbose) {
                                echo PHP_EOL.'dumping '.$table->getName().PHP_EOL;
                            }
                            $this->exportPartialData($table);
                        }
                    } // else, clone all data of this table ...
                    elseif (in_array($table->getName(), $this->config['fullTablesData'])
                        || $this->config['allData'] && !in_array($table->getName(), $this->config['ignoreTablesData'])) {
                        if ($this->verbose) {
                            echo PHP_EOL.'export all data of table '.$table->getName().PHP_EOL;
                        }
                        $this->exportFullData($table);
                    }
                }
            }
            $end = microtime(true);
            $duration = $end - $start;

            echo PHP_EOL.sprintf('Total exported rows: %s in %.1f seconds', number_format($this->totalExportedRows, 0, '.', ' '), $duration).PHP_EOL;
        } catch (TooDeepException $tde) {
            echo $tde->getMessage().PHP_EOL;
            echo 'stack: '.PHP_EOL;
            foreach ($tde->getStack() as $table) {
                echo ' '.$table.PHP_EOL;
            }
        }
    }

    /**
     * Append before table data
     */
    public function appendBeforeTableData()
    {
        // start transaction
        file_put_contents(
            $this->file,
            "SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET AUTOCOMMIT = 0;
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;
",
            FILE_APPEND
        );
    }

    /**
     * Append ater table data
     */
    public function appendAfterTableData()
    {
        // commit transaction
        file_put_contents(
            $this->file,
            "COMMIT;
",
            FILE_APPEND
        );
    }

    /**
     * Export partial data
     *
     * @param Table $table
     *
     * @throws Exception
     */
    public function exportPartialData(Table $table)
    {
        $this->appendBeforeTableData();

        foreach ($this->config['limitTables'][$table->getName()]['queries'] as $query) {
            // pre-filtering of results ?
            if (isset($query['query'])) {
                // if query_params
                if (isset($query['query_params'])) {
                    $queryParams = $this->loadQueryParams($query['query_params']);
                } else {
                    $queryParams = [];
                }

                $sql = $this->mergeArrayParams($query['query'], $queryParams);
                $rows = $this->link->prepare($sql);
                $rows->execute($queryParams);
                $this->nbQueries++;
                if ($this->verbose) {
                    echo $rows->rowCount().' rows to dump'.PHP_EOL;
                }
                $this->sectionRowsToDump = $rows->rowCount();
                $this->sectionRowsDumped = 0;
                foreach ($rows->fetchAll(PDO::FETCH_ASSOC) as $row) {
                    $this->exportQueryData($table, $query, $row);
                    $this->sectionRowsDumped++;
                }
            } else {
                $this->exportQueryData($table, $query, []);
            }
        }

        $this->appendAfterTableData();
        $this->printProgress([]);
    }

    /**
     * Export full table data (with anonymization filters)
     *
     * @param Table $table
     *
     * @throws Exception
     */
    public function exportFullData(Table $table)
    {
        $this->appendBeforeTableData();

        $sql = 'SELECT * FROM `'.$table->getName().'`';
        $rows = $this->link->prepare($sql);
        $rows->execute([]);
        $this->nbQueries++;
        $this->sectionRowsDumped=0;
        $this->sectionRowsToDump=$rows->rowCount();
        foreach ($rows->fetchAll(PDO::FETCH_ASSOC) as $row) {
            $this->exportRow($table, $row, [$table->getName()]);
            $this->sectionRowsDumped++;
        }

        $this->printProgress([]);

        // commit transaction
        file_put_contents(
            $this->file,
            "COMMIT;
",
            FILE_APPEND
        );
    }

    /**
     * Load query params
     *
     * @param array $queryParams
     *
     * @return array
     */
    public function loadQueryParams($queryParams)
    {
        $params = [];

        foreach ($queryParams as $queryParamName => $queryParamConf) {
            $queryParamConf += [
                'type' => '',
            ];

            switch ($queryParamConf['type']) {
                // use raw query results ids
                case 'query':
                    // load all id
                    $rows = $this->link->prepare($queryParamConf['query']);
                    $rows->execute();
                    $this->nbQueries++;
                    $params[$queryParamName] = $rows->fetchAll(PDO::FETCH_COLUMN, 0);
                    break;
                // use already exported ids
                case 'exported_id':
                    // add exported id
                    if (!isset($this->exportedId[$queryParamConf['table']])) {
                        throw new InvalidArgumentException('missing table '.$queryParamConf['table'].' in exported_id query type');
                    }
                    $params[$queryParamName] = array_keys($this->exportedId[$queryParamConf['table']]);

                    break;
                default:
                    throw new InvalidArgumentException('missing type in query_params');
            }
        }

        return $params;
    }

    /**
     * Prepare a query, merging arrays params into sql
     *
     * @param string $sql
     * @param array  $params (merged params will be removed from this array)
     *
     * @return string the sql query with merged array params
     */
    public function mergeArrayParams($sql, &$params)
    {
        // quick hack: handle array params (for IN statement) before PDO, because PDO don't handle arrays
        foreach ($params as $key => $value) {
            // only handle arrays
            if (is_array($value)) {
                $sql = str_replace(':'.$key, implode(',', $value), $sql);
                // and remove this parameter from the list for PDO
                unset($params[$key]);
            }
        }

        return $sql;
    }

    /**
     * Export query
     *
     * @param Table $table
     * @param array $query
     * @param array $queryParams
     *
     * @throws Exception
     */
    public function exportQueryData(Table $table, $query, $queryParams)
    {
        if ($this->verbose >= 2) {
            echo "exportQueryData - ".$this->config['database'].'.'.$table->getName().PHP_EOL;
        }

        // handle join on other tables
        if (isset($query['join'])) {
            $join = ' '.$query['join'];
        } else {
            $join = '';
        }

        // handle having
        if (isset($query['having'])) {
            $having = ' HAVING '.$query['having'];
        } else {
            $having = '';
        }

        // limit results
        if (isset($query['limit'])) {
            $limit = ' LIMIT 0,'.max(1, $query['limit']);
        } else {
            $limit = '';
        }

        $sql = "SELECT ".$table->getName().".* FROM ".$table->getName().$join." WHERE ".$query['where'].$having.$limit;

        if ($this->verbose >= 4) {
            echo $sql.PHP_EOL;
        }

        // query params
        $params = $queryParams;
        if (isset($query['where_params'])) {
            $params = array_merge($params, $this->loadQueryParams($query['where_params']));
        }
        $sql = $this->mergeArrayParams($sql, $params);
        $rows = $this->link->prepare($sql);

        // and launch the final query, with others parameters escaped
        $rows->execute($params);
        $this->nbQueries++;

        if (isset($query['where_params']) && $this->verbose >= 4) {
            echo $rows->queryString.PHP_EOL;
        }

        foreach ($rows->fetchAll(PDO::FETCH_ASSOC) as $row) {
            $this->exportRow($table, $row, [$table->getName()]);
        }
    }

    /**
     * Check if a table id is already exported
     *
     * @param Table $table
     * @param mixed $id
     *
     * @return bool true if already exported
     */
    public function isExportedId(Table $table, $id)
    {
        if (!isset($this->exportedId[$table->getName()][$id])) {
            return false;
        }

        return $this->exportedId[$table->getName()][$id];
    }

    /**
     * Export a row (recursive function)
     *
     * @param Table $table
     * @param array $row
     * @param array $path tables already scanned in the recursive call
     *
     * @throws Exception
     */
    public function exportRow(Table $table, $row, $path = [])
    {
        if (count($path) >= $this->getMaxDeep()) {
            throw new TooDeepException(sprintf('max deep %d reached. use --max-deep to override', $this->getMaxDeep()), 500, null, $path);
        }

        $idKey = $table->getPrimaryKeyIdAsString($row);
        $idKeyText = $table->getPrimaryKeyIdAsString($row);

        if ($this->verbose >= 3) {
            echo str_repeat('-', count($path))." ".$this->config['database'].'.'.$table->getName().' '.$idKeyText.PHP_EOL;
        }

        if (!$this->isExportedId($table, $idKey)) {
            // if we need to overwrite some values, like passwords
            if (isset($this->config['forceValues'][$table->getName()])) {
                foreach ($this->config['forceValues'][$table->getName()] as $key => $value) {
                    $row[$key] = $value;
                }
            }
            // write SQL INSERT
            file_put_contents($this->file, $this->rowToInsert($table->getName(), $row), FILE_APPEND);
            // mark this row exported
            $this->exportedId[$table->getName()][$idKey] = true;
            $this->nbExportedRows++;
            $this->totalExportedRows++;

            // simple visualisation progress
            if (0 === $this->nbExportedRows % 97) {
                $this->printProgress($path);
            }

            // scanning FK
            foreach ($table->getForeignKeys() as $fk) {
                $foreignId = $row[$fk->getField()];
                if (null !== $foreignId && 0 !== $foreignId) {
                    $foreignTable = $this->schema[$fk->getForeignTable()];
                    $this->exportQueryRows($foreignTable, $fk->getForeignField().' = :id', ['id' => $foreignId], $path);
                }
            }
            // scanning reverse FK
            foreach ($table->getReverseForeignKeys() as $rfk) {
                // table not ini already walked path and not ignored ?
                if (
                    !in_array($rfk->getTable(), $this->config['ignoreTablesData'])
                    && !in_array($rfk->getTable(), $this->config['noReverseTables'])
                    && !in_array($rfk->getTable(), $path)
                    && !$this->isNoReverseLink($rfk)
                    && !$table->isPrimaryKeyComposed()
                ) {
                    try {
                        $this->exportQueryRows($this->schema[$rfk->getTable()], $rfk->getField().' = :id', ['id' => $idKey], $path);
                    } catch (TooManyResultsException $e) {
                        $this->tooManyResults[] = [
                            'exception' => $e,
                            'path' => $path,
                            'table' => $rfk->getTable(),
                            'field' => $rfk->getField(),
                        ];
                        if ($this->verbose >= 2) {
                            echo $e->getMessage().PHP_EOL;

                            echo 'Stack: '.PHP_EOL;
                            foreach ($path as $tablePath) {
                                echo ' '.$tablePath.PHP_EOL;
                            }
                        }

                        echo sprintf('%d results in last call, limit: %d (use --max-revert-rows to accept more results)', $e->getNbResults(), $this->getMaxRevertRows()).PHP_EOL;
                        echo 'You should probably ignore this link adding the following line to the noReverseLinks directive: '.PHP_EOL.PHP_EOL;
                        echo " '".$rfk->getTable().".".$rfk->getField()."',".PHP_EOL.PHP_EOL;
                        echo 'This link is now ignored for this dump.'.PHP_EOL.PHP_EOL;

                        $this->config['noReverseLinks'][] = $rfk->getTable().".".$rfk->getField();
                    }
                }
            }
        }
    }

    /**
     * Check if a link is in the no reverse link table
     *
     * @param ForeignKey $fk
     * @return bool true if the link is found
     */
    public function isNoReverseLink(ForeignKey $fk)
    {
        $fkName = $fk->getTable().'.'.$fk->getField();

        return in_array($fkName, $this->config['noReverseLinks']);
    }

    /**
     * Export query rows
     *
     * @param Table  $table
     * @param string $where
     * @param array  $whereParams
     * @param array  $path tables already scanned in the recursive call
     *
     * @throws Exception
     */
    public function exportQueryRows(Table $table, $where, $whereParams, $path)
    {
        $path[] = $table->getName();

        if ($this->verbose >= 2) {
            echo str_repeat('-', count($path))." ".$this->config['database'].'.'.$table->getName().' ('.implode(',', $whereParams).')'.PHP_EOL;
        }

        $rows = $this->link->prepare("SELECT * FROM ".$table->getName()." WHERE ".$where);
        $rows->execute($whereParams);
        $this->nbQueries++;

        if ($this->verbose >= 4) {
            echo str_repeat('-', count($path)).' '.$rows->rowCount().' results'.PHP_EOL;
        }
        if ($rows->rowCount() > $this->getMaxRevertRows()) {
            throw new TooManyResultsException('too many results', 500, null, $rows->rowCount());
        }
        foreach ($rows->fetchAll(PDO::FETCH_ASSOC) as $row) {
            $this->exportRow($table, $row, $path);
        }

        array_pop($path);
    }

    /**
     * Row to INSERT
     *
     * @param string $table name of table
     * @param array  $row associative array
     *
     * @return string
     */
    public function rowToInsert($table, $row)
    {
        $this->anonymizeRow($table, $row);

        $sql = "INSERT INTO `".$table."` (";
        $i = 0;
        foreach ($row as $key => $notused) {
            if ($i > 0) {
                $sql .= ',';
            }
            $sql .= '`'.$key.'`';
            $i++;
        }
        $sql .= " ) VALUES (";
        $i = 0;
        foreach ($row as $value) {
            if ($i > 0) {
                $sql .= ',';
            }
            if (null === $value) {
                $sql .= 'null';
            } else {
                $sql .= $this->link->quote($value);
            }
            $i++;
        }
        $sql .= ");".PHP_EOL;

        return $sql;
    }

    /**
     * Anonymize a row
     *
     * @param string $table name of table
     * @param array  $row
     */
    public function anonymizeRow($table, &$row)
    {
        foreach ($row as $key => $value) {

            $filter = null;
            // if field should be anonymized
            if (isset($this->config['anonymizer']['fields'][$key])) {
                $filter = $this->config['anonymizer']['fields'][$key];
            } // @todo handle specific table anonymization
            elseif (isset($this->config['anonymizer']['tables'][$table]['fields'][$key])) {
                $filter = $this->config['anonymizer']['tables'][$table]['fields'][$key];
                // exception to keep some values ?
                if (isset($this->config['anonymizer']['tables'][$table]['exceptions'][$key])
                    && in_array($value, $this->config['anonymizer']['tables'][$table]['exceptions'][$key])
                ) {
                } // need to be anonymized
                else {
                    $row[$key] = $this->anonymizeValue($value, $this->config['anonymizer']['tables'][$table]['fields'][$key]);
                }
            }
            if (null !== $filter) {
                // exception to keep some values ?
                if (isset($this->config['anonymizer']['tables'][$table]['exceptions'][$key])
                    && in_array($value, $this->config['anonymizer']['tables'][$table]['exceptions'][$key])
                ) {
                    // just skip filtering
                } // need to be anonymized
                else {
                    $row[$key] = $this->anonymizeValue($value, $filter);
                }
            }
        }
    }

    /**
     * Anonymize a value
     *
     * @param string $value
     * @param string $anonymizer
     *
     * @return string
     */
    public function anonymizeValue($value, $anonymizer)
    {
        switch ($anonymizer) {
            case self::ANONYMIZER_ALPHA_NUM:
                return $this->anonymizeValue($this->anonymizeValue($value, 'ALPHA'), 'NUM');
            case self::ANONYMIZER_NUM:
                return strtr($value, $this->anonymizeNum);
            // preserve first 4 characters
            case self::ANONYMIZER_PHONE:
                $prefix = substr($value, 0, 4);
                $last = substr($value, 4);

                return $prefix .= $this->anonymizeValue($last, 'NUM');
            case self::ANONYMIZER_LOREM_IPSUM:
                return LoremIpsumFilter::filter($value);
            case self::ANONYMIZER_ALPHA:
            default:
                return strtr($value, $this->anonymize);
        }
    }

    /**
     * @param int $maxRevertRows
     */
    public function setMaxRevertRows($maxRevertRows)
    {
        $this->maxRevertRows = $maxRevertRows;
    }

    /**
     * @return int
     */
    public function getMaxRevertRows()
    {
        return $this->maxRevertRows;
    }

    /**
     * @return int
     */
    public function getMaxDeep()
    {
        return $this->maxDeep;
    }

    /**
     * @param int $maxDeep
     */
    public function setMaxDeep($maxDeep)
    {
        $this->maxDeep = $maxDeep;
    }

    /**
     * @param array $path
     */
    private function printProgress($path)
    {
        echo sprintf(
            "\rsection progress: %.1f%% - %s queries, %s / %s main rows dumped (section), %s exported rows, %s total rows%30s",
            $this->sectionRowsToDump == 0 ? 0 : 100.0 * $this->sectionRowsDumped / $this->sectionRowsToDump,
            number_format($this->nbQueries, 0, '.', ' '),
            number_format($this->sectionRowsDumped, 0, '.', ' '),
            number_format($this->sectionRowsToDump, 0, '.', ' '),
            number_format($this->nbExportedRows, 0, '.', ' '),
            number_format($this->totalExportedRows, 0, '.', ' '),
            ""
        );
    }

}
