<?php

class Table
{

    /**
     * Table name
     *
     * @var string
     */
    private $name;

    /**
     * Primary key fields
     *
     * @var array
     */
    private $primaryKey = [];

    /**
     * Foreign keys
     *
     * @var ForeignKey[]
     */
    private $foreignKeys = [];

    /**
     * Reverse Foreign keys
     *
     * @var ForeignKey[]
     */
    private $reverseForeignKeys = [];

    /**
     * @param mixed $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param ForeignKey[] $foreignKeys
     */
    public function setForeignKeys($foreignKeys)
    {
        $this->foreignKeys = $foreignKeys;
    }

    /**
     * @return ForeignKey[]
     */
    public function getForeignKeys()
    {
        return $this->foreignKeys;
    }

    /**
     * @param ForeignKey $key
     */
    public function addForeignKey(ForeignKey $key)
    {
        $this->foreignKeys[] = $key;
    }

    /**
     * @param ForeignKey[] $foreignKeys
     */
    public function setReverseForeignKeys($foreignKeys)
    {
        $this->reverseForeignKeys = $foreignKeys;
    }

    /**
     * @return ForeignKey[]
     */
    public function getReverseForeignKeys()
    {
        return $this->reverseForeignKeys;
    }

    /**
     * @param ForeignKey $key
     */
    public function addReverseForeignKey(ForeignKey $key)
    {
        $this->reverseForeignKeys[] = $key;
    }

    /**
     * Add a field to the primary key
     *
     * @param string $field
     */
    public function addPrimaryKeyField($field)
    {
        $this->primaryKey[] = $field;
    }

    /**
     * Get primary key fields
     *
     * @return array
     */
    public function getPrimaryKeyFields()
    {
        return $this->primaryKey;
    }

    /**
     * Extract primary key id as string
     *
     * @param $row
     *
     * @return string
     */
    public function getPrimaryKeyIdAsString($row)
    {
        $keyId = '';
        foreach ($this->getPrimaryKeyFields() as $field) {
            if ($keyId != '') {
                $keyId .= '-';
            }
            $keyId .= $row[$field];
        }

        return $keyId;
    }

    /**
     * Extract primary key as string (with fields=value)
     *
     * @param $row
     *
     * @return string
     */
    public function getPrimaryKeyAsString($row)
    {
        $key = '';

        foreach ($this->getPrimaryKeyFields() as $field) {
            if ($key != '') {
                $key .= ';';
            }
            $key .= $field.'-'.$row[$field];
        }

        return $key;
    }

    /**
     * @return bool
     */
    public function isPrimaryKeyComposed()
    {
        return count($this->primaryKey) > 1;
    }
}

