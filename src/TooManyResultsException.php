<?php

class TooManyResultsException extends Exception
{

    /**
     * @var int
     */
    private $nbResults;

    /**
     * @param string    $message [optional] The Exception message to throw.
     * @param int       $code [optional] The Exception code.
     * @param Throwable $previous [optional] The previous throwable used for the exception chaining.
     * @param int       $nbResults
     */
    public function __construct($message = "", $code = 0, Throwable $previous = null, $nbResults)
    {
        parent::__construct($message, $code, $previous);

        $this->nbResults = $nbResults;
    }

    /**
     * @return int
     */
    public function getNbResults()
    {
        return $this->nbResults;
    }
}
