Mysql Filter Dump
=================

mysql-filter-dump is a tool to export partial data (with foreign keys) of a dump.

This project has a git flow to build and push images on docker registry.

Usage
=====

You only needs mysql-filter-dump script (use it with ansible).

```shell
./mysql-filter-dump <config-path> <dump-path> <mydatabasename>
```

To work with this project (dev)
===============================

```sh
cd ~/PhpstormProjects
git clone git@gitlab.com:kilik/tools/mysql-filter-dump.git MysqlFilterDump
cd MysqlFilterDump
make build
```

Update your config:

```sh
cp config/config.dist.php config/config.php
nano config/config.php
```

And launch your dump (a file should be created: dumps_path/{currentdate}/connection_name.dump_name.sql

```sh
./mysql-filter-dump config_path dumps_path dump_name
```

Ex:

```sh
./mysql-filter-dump config ~/dumps mydump
```

Then, integrate this dump with a progress bar:

```sh
cd ~/dumps/{currentdate}
pv myserver.mydump.sql | mysql -u root -p mylocaldatabase
```

Filters
=======

* ALPHA: replace alpha latin1 (lower or upper case)
* NUM: replace numbers
* ALPHA_NUM: replace alpha latin1 + numbers
* PHONE: replace numbers, keeping phone prefix
* LOREM_IPSUM: replace a medium text with one of 16 lorem ipsum texts
