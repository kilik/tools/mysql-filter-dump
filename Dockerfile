FROM debian:9.6-slim

RUN DEBIAN_FRONTEND=noninteractive apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get upgrade -y && \
    DEBIAN_FRONTEND=noninteractive apt-get install -y --no-install-recommends \
    mysql-client pv php-cli php-mysqli

RUN DEBIAN_FRONTEND=noninteractive phpenmod -v all mysqli

ENV PHP_UID 33
ENV PHP_GID 33

COPY entrypoint.sh /
COPY mysqldump.cnf /etc/mysql/conf.d/mysqldump.cnf
RUN chmod 0444 /etc/mysql/conf.d/mysqldump.cnf && \
    chown www-data:www-data /entrypoint.sh && \
    usermod -s /bin/bash www-data && \
    mkdir -p /var/www && chown www-data:www-data /var/www && \
    mkdir -p /dumps

#USER www-data
ADD . /usr/src/myapp

WORKDIR /usr/src/myapp
CMD [ "/entrypoint.sh" ]
