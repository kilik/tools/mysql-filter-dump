<?php

class config
{

    public $connections
        = [
            'localhost' => [
                'host' => '127.0.0.1',
                'port' => 3306,
                'user' => 'root',
                'password' => 'test',
            ],
            'server01' => [
                'host' => 'server01.tld',
                'port' => 3306,
                'user' => 'reader',
                'password' => '****',
            ],
            'server02' => [
                'host' => 'server02.tld',
                'port' => 3306,
                'user' => 'dump',
                'password' => '****',
            ],
        ];

    public $dumps
        = [
            'mydatabase-default' => [
                'database' => 'mydatabase',
                'connection' => 'server01',
                // don't follow those reverse links
                'noReverseLinks' => [
                    'mytable.myfield',
                ],
                // extract filtered rows (and linked rows)
                'limitTables' => [
                    // for this table
                    'document' => [
                        // make this queries
                        'queries' => [
                            [
                                // get 2 documents MAX for each Agency,Package and Date (day) since 2018-07-01
                                'query' => "SELECT d.id_agency,d.id_package,c.date AS period FROM document AS c WHERE c.date > '2018-07-01 00:00:00' GROUP BY d.id_agency,d.id_package,period",
                                'where' => "id_agency = :id_agency AND id_package = :id_package AND date = :period",
                                'limit' => 2,
                            ],
                        ],
                    ],
                ],
                // not implemented yet
                'fullTablesData' => [],
                // ignore tables from schema
                'ignoreTables' => [
                    // ignore log table
                    'log',
                ],
                // force table.field values
                'forceValues' => [
                    'users' => [
                        // force password hash "test"
                        'password' => 'a94a8fe5ccb19ba61c4c0873d391e987982fbbd3',
                    ],
                ],
                // force primary keys (when PRIMARY keys are missing in database schema)
                'forcePrimaryKeys' => [
                    'mybadtable' => [
                        'my_bad_primary_key',
                    ],
                    // or for composed keys
                    'mybadtable2' => [
                        'id_my_bad_primary_key1',
                        'id_my_bad_primary_key2',
                    ],
                ],
                // force foriegn keys (when FK are missing in database schema)
                'forceForeignKeys' => [
                    'mybadtable' => [
                        'id_option' => ['foreigntable', 'id'],
                        'id_field' => ['othertable', 'id'],
                    ],
                ],
                // destroy some data
                'anonymizer' => [
                    // by field name => method
                    'fields' => [
                        'first_name' => 'ALPHA',
                        'last_name' => 'ALPHA',
                        'email' => 'ALPHA',
                        'phone_number' => 'PHONE',
                        'contract_number' => 'NUM',
                        'address' => 'ALPHA_NUM',
                    ],
                    // or by table field
                    'users' => [
                        'fields' => [
                            'login' => 'ALPHA',
                            'name' => 'ALPHA',
                            'email' => 'ALPHA',
                        ],
                        // make exception to keep some usefull users
                        'exceptions' => [
                            'login' => [
                                'admin',
                                'mnaud',
                            ],
                        ],
                    ],

                ],
            ],
        ];
}
