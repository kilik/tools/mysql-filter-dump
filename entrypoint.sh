#!/bin/bash
usermod -u $PHP_UID www-data
groupmod -g $PHP_GID www-data
runuser -l www-data -c 'cd /usr/src/myapp; /usr/bin/env php main.php $0 $@' -- "$@"
